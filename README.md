### Assuming a uniform distribution within a unit circle....
1. Average distance of all points from center of circle?
2. Average distance of all points from one point on circumference?
3. Average distance of all points to nearest point on circumference?
4. Average distance between two random points in circle? <-- _This was the original question_

I generated n = 1000 points uniformly over a disk.. 
I also generated nC = 1000 points over the circumference of this disk. 
Generating more should yield more precise results but the increased computation time and higher-density plots do not seem worth it.

Here are those n points:

![Generated points](GeneratedPoints.png)


Here's a cool visualization of Question 3:

![3](AvgDistInCircle.png)

```
Average distance of all points from center of circle: 0.661 +- 0.007
Average distance of all points to single point on circumference of circle (1,0): 1.1 +- 0.02
Average distance of all points to nearest point on circumference of circle: 0.54 +- 0.01
Average distance between two random points in circle: 0.91 +- 0.02
```

As far as I've found, these results are correct or very close to correct (numerically, anyway).

Note: I apologize for the sparse explaination here. I am only uploading this old code, not re-diving into it. 
Please view the code itself for more specific documentation.
