import ROOT
import numpy as np
import math as math
import os
import linecache
import sys

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();
print ''





# Edit to fix rejected commit.


### Assumine uniform distributions.
# 1. Average distance of all points from center of circle?
# 2. Average distance of all points from one point on circumference?
# 3. Average distance of all points to nearest point on circumference?
# 4. Average distance between two random points in circle?









# http://mathworld.wolfram.com/DiskPointPicking.html
# http://mathworld.wolfram.com/CirclePointPicking.html
# http://stackoverflow.com/questions/27606079/method-to-uniformly-randomly-populate-a-disk-with-points-in-python

# Unit circle centered at (0,0).
R = 1.								# Radius of circle
x0 = 0.
y0 = 0.

# Generate rando point locations within circle bounds.
n = 1000							# number of points
print ' ', n, 'areal points'
r = np.random.uniform(low=0., high=R, size=n)			# radius of point
theta = np.random.uniform(low=0., high=2.*np.pi, size=n)	# angle of point

# Location of points in rectangular coords.
x = x0 + sqrt(r)*np.cos(theta)
y = y0 + sqrt(r)*np.sin(theta)


### Plot rando points
c = ROOT.TCanvas('c', 'c', 720, 720)
g = ROOT.TGraph(len(x), x, y)
g.GetXaxis().SetLimits(-R, R)
g.GetYaxis().SetRangeUser(-R, R)
g.SetTitle('n = '+'{:,}'.format(n)+';;')
g.SetMarkerStyle(ROOT.kFullDotLarge)
g.SetMarkerColor(ROOT.kViolet)
g.SetMarkerSize(0.5)
g.Draw('ap')
# Draw center of circle
g0 = ROOT.TGraph(1, np.array(x0), np.array(y0))
g0.SetMarkerStyle(ROOT.kFullDotLarge)
g0.SetMarkerSize(0.25)
g0.Draw('same p')
# Draw circle/circumference
'''
circ = ROOT.TEllipse(x0,y0,R,R)
circ.SetFillStyle(0)
circ.Draw()
'''
c.SaveAs('GeneratedPoints.png')












### 1. Average distance of all points from center of circle?
rAvg1 = np.mean(sqrt(r)) # This method may be quicker than the belows.
# == sum([sqrt((X-x0)**2 + (Y-y0)**2) for X,Y in zip(x,y)])/n 
# == np.mean([sqrt((X-x0)**2 + (Y-y0)**2) for X,Y in zip(x,y)])
rAvg1Unc = np.std(sqrt(r))/sqrt(n)
# == np.std([sqrt((X-x0)**2 + (Y-y0)**2) for X,Y in zip(x,y)])/sqrt(n) #sqrt(n)/n

# Rounding digits
# From http://stackoverflow.com/questions/8011017/python-find-first-non-zero-digit-after-decimal-point
digits1 = abs(int(math.log10(abs(rAvg1Unc))))+1 # To nearest non-zero figure. 
print 'Average distance of all points from center of circle:', round(rAvg1,digits1), '+-', round(rAvg1Unc,digits1)













### 2. Average distance of all points from one point on circumference?
# Let this point (1,0). Note that setting this to (0,0) returns same result as above.
x1 = 1.
y1 = 0.
rAvg2 = sum([sqrt((X-x1)**2 + (Y-y1)**2) for X,Y in zip(x,y)])/n
rAvg2Unc = np.std([sqrt((X-x1)**2 + (Y-y1)**2) for X,Y in zip(x,y)])/sqrt(n)

digits2 = abs(int(math.log10(abs(rAvg2Unc))))+1
print 'Average distance of all points to single point on circumference of circle (1,0):', round(rAvg2,digits2), '+-', round(rAvg2Unc,digits2)













### 3. Average distance of all points to nearest point on circumference?
# https://en.wikipedia.org/wiki/Closest_pair_of_points_problem

#Points on circumference
nC = n #1000
print ' ', nC, 'circumferential points'
thetaC = np.random.uniform(low=0., high=2.*np.pi, size=nC)        # angle of point
xC = x0 + sqrt(R) * np.cos(thetaC)
yC = y0 + sqrt(R) * np.sin(thetaC)

### Plot rando circumference points
#cC = ROOT.TCanvas('cC', 'cC', 720, 720)
gC = ROOT.TGraph(len(xC), xC, yC)
gC.GetXaxis().SetLimits(-R, R)
gC.GetYaxis().SetRangeUser(-R, R)
gC.SetMarkerStyle(ROOT.kFullDotLarge)
gC.SetMarkerSize(0.25)
gC.Draw('same p')
#g0.Draw('same p')


### Find minimum distance between point and nearest point on circumference.
# Problem with putting arrays into arrays...
'''
coordsArray = np.zeros(n)
for i,(xx,yy) in enumerate(zip(x,y)):
  mindd = 1.
  for xxC,yyC in zip(xC, yC):
    dd = sqrt((xxC-xx)**2 + (yyC-yy)**2)
    if dd < mindd:
      mindd = dd
      coordsArray[i] = [xx,yy,xxC,yyC] #[(xx,yy),(xxC,yyC)]
'''
# Works, but appends several (varying number) of points until it reaches minimum, resulting in several points (drawn)...
'''
coordsArray = []
for xx,yy in zip(x,y):
  mindd = 1.
  for xxC,yyC in zip(xC, yC):
    dd = sqrt((xxC-xx)**2 + (yyC-yy)**2)
    if dd < mindd:
      mindd = dd
      coordsArray.append([(xx,yy),(xxC,yyC)]) #mindd)
  #coordsArray.append(mindd)
'''
# Works. Perhaps my variable names suck...
distArray = []
coordsArray = []
for xx,yy in zip(x,y):
  mindd = 1.
  for xxC,yyC in zip(xC, yC):
    dd = sqrt((xxC-xx)**2 + (yyC-yy)**2)
    if dd < mindd:
      mindd = dd
      coordsArrayi = [(xx,yy),(xxC,yyC)]
  distArray.append(mindd)
  coordsArray.append(coordsArrayi)

line = ROOT.TLine()
line.SetLineColor(ROOT.kGray+2)
line.SetLineStyle(ROOT.kDashed)
for i in coordsArray:
  line.DrawLine(i[0][0], i[0][1], i[1][0], i[1][1])
g.Draw('same p') # Draw again on top
  

rAvg3 = sum(sqrt(distArray))/n
#rAvg3Unc = np.std(sqrt(distArray))/sqrt(n)
rAvg3Unc = 2.*np.std(sqrt(distArray))/sqrt(n) # Letting 2.* represent the uncertainty for circumference resolution...
#rAvg3Unc = sqrt(2.)*np.std(sqrt(distArray))/sqrt(n) # Letting sqrt(2.)* represent the uncertainty for circumference resolution...

digits3 = abs(int(math.log10(abs(rAvg3Unc))))+1
print 'Average distance of all points to nearest point on circumference of circle:', round(rAvg3,digits3), '+-', round(rAvg3Unc,digits3)








c.SaveAs('AvgDistInCircle.png')











### 4. Average distance between two random points in circle?
# https://math.stackexchange.com/questions/135766/average-distance-between-two-points-in-a-circular-disk
# Difference between i and i+1 indices of x, used as two random points. Effectively n/2 samples.
x2 = [x[2*i]+x[2*i+1] for i in range(len(x)/2)]
y2 = [y[2*i]+y[2*i+1] for i in range(len(y)/2)]

rAvg4 = sum([sqrt(Y**2 + X**2) for X,Y in zip(x2,y2)])/(n/2.)
rAvg4Unc = np.std([sqrt(X**2 + Y**2) for X,Y in zip(x2,y2)])/sqrt(n/2.)

digits4 = abs(int(math.log10(abs(rAvg4Unc))))+1
print 'Average distance between two random points in circle:', round(rAvg4,digits4), '+-', round(rAvg4Unc,digits4)
print ''
